package main

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

type SteamGame struct {
	ApplicationId string `xml:"appID"`
	Name string `xml:"name"`
	Logo string `xml:"logo"`
	StoreLink string `xml:"storeLink"`
	PlayTimePast2Weeks string `xml:"hoursLast2Weeks"`
	PlayTimeTotal string `xml:"hoursOnRecord"`
}

type ArrayGames struct {
	Game []SteamGame `xml:"game"`
}

type XmlSteamGamesList struct {
	SteamId64 string `xml:"steamID64"`
	Username string `xml:"steamID"`
	Games ArrayGames `xml:"games"`
}

func getDataFromUrl(url string) ([]byte, error) {
	resp, err := http.Get(url)
	if err != nil {
		return []byte{}, fmt.Errorf("GET error: %v", err)
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusOK {
		return []byte{}, fmt.Errorf("Status error: %v", resp.StatusCode)
	}

	data, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return []byte{}, fmt.Errorf("Read body: %v", err)
	}

	return data, nil
}

func GetGamesFromProfileId(profileId string) (error, string, []SteamGame) {
	// A partir du SteamID, on va récupérer la liste des jeux

	if xmlBytes, err := getDataFromUrl(fmt.Sprintf("https://steamcommunity.com/profiles/%s/games/?tab=all&xml=1", profileId)); err != nil {
		log.Printf("Failed to get XML: %v", err)
	} else {
		var result XmlSteamGamesList
		err = xml.Unmarshal(xmlBytes, &result)
		if err != nil {
			return err, "", nil
		}
		return nil, result.Username, result.Games.Game
		fmt.Printf("%+v\n", result)
	}
	return nil, "", nil
}

type JsonGamePriceOverview struct {
	Currency string `json:"currency"`
	Initial int `json:"initial"`
	Final int `json:"final"`
	DiscountPercent int `json:"discount_percent"`
	InitialFormatted string `json:"initial_formatted"`
	FinalFormatted string `json:"final_formatted"`
}

type JsonGamePlatforms struct {
	Windows bool `json:"windows"`
	Mac bool `json:"mac"`
	Linux bool `json:"linux"`
}

type JsonGameCategory struct {
	Id int `json:"id"`
	Description string `json:"description"`
}
type JsonGameGenre struct {
	Id string `json:"id"`
	Description string `json:"description"`
}

type JsonGameRelease struct {
	ComingSoon bool `json:"coming_soon"`
	Date string `json:"date"`
}

type JsonGameData struct {
	Type string `json:"type"`
	Name string `json:"name"`
	Free bool `json:"is_free"`
	Languages string `json:"supported_languages"`
	HeaderImage string `json:"header_image"`
	PriceOverview JsonGamePriceOverview `json:"price_overview"`
	Platforms JsonGamePlatforms `json:"platforms"`
	Categories []JsonGameCategory `json:"categories"`
	Genres []JsonGameGenre `json:"genres"`
	ReleaseDate JsonGameRelease `json:"release_date"`
}

type JsonGameInfo struct {
	Success bool `json:"success"`
	Data JsonGameData `json:"data"`
}

func (game *SteamGame) GetInfo() (error, JsonGameData) {
	if jsonBytes, err := getDataFromUrl(fmt.Sprintf("https://store.steampowered.com/api/appdetails?appids=%s", game.ApplicationId)); err != nil {
		log.Printf("Failed to get Json: %v", err)
	} else {
		var result map[string]JsonGameInfo
		err = json.Unmarshal(jsonBytes, &result)
		if err != nil {
			return err, JsonGameData{}
		}
		//fmt.Printf("%+v\n", result)
		return nil, result[game.ApplicationId].Data
	}
	return nil, JsonGameData{}
}