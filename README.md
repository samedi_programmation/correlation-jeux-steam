############################
# Les Samedi Programmation #
############################


Samedi 17/10/2020 : Liste de jeux Steam
=======================================


Ce code source est la base du live sur la récupération de listes de jeux Steam.


Il montre un exemple de gestion de la liste des jeux via Steam.

- Récupération du SteamID
- Récupération de la liste des jeux d'une personne
- Comparaisons & tri de liste
- Récupération des informations d'un jeu spécifique


Si vous avez plus de questions, vous pouvez me contacter sur https://twitch.tv/adaralex

Bonne journée à vous,

Adaralex