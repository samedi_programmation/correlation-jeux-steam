package main

import (
	"encoding/json"
	"fmt"
	"github.com/yohcop/openid-go"
	"html/template"
	"log"
	"net/http"
	"sort"
	"strings"
)

const domain = "localhost"
const port = ":8080"

type SteamOwnershipInfo struct {
	GameInfo  SteamGame
	GameData  JsonGameData
	Ownership []string
}

// Load the templates once
var templateDir = "./"
var indexTemplate = template.Must(template.ParseFiles(templateDir + "index.html"))

var ownershipInfo map[string]SteamOwnershipInfo

// NoOpDiscoveryCache implements the DiscoveryCache interface and doesn't cache anything.
// For a simple website, I'm not sure you need a cache.
type NoOpDiscoveryCache struct{}

// Put is a no op.
func (n *NoOpDiscoveryCache) Put(id string, info openid.DiscoveredInfo) {}

// Get always returns nil.
func (n *NoOpDiscoveryCache) Get(id string) openid.DiscoveredInfo {
	return nil
}

var nonceStore = openid.NewSimpleNonceStore()
var discoveryCache = &NoOpDiscoveryCache{}

// indexHandler serves up the index template with the "Sign in through STEAM" button.
func indexHandler(w http.ResponseWriter, r *http.Request) {
	indexTemplate.Execute(w, nil)
}

// discoverHandler calls the Steam openid API and redirects to steam for login.
func discoverHandler(w http.ResponseWriter, r *http.Request) {
	url, err := openid.RedirectURL(
		"http://steamcommunity.com/openid",
		"http://"+domain+port+"/openidcallback",
		"http://"+domain+port+"/")

	if err != nil {
		log.Printf("Error creating redirect URL: %q\n", err)
	} else {
		http.Redirect(w, r, url, 303)
	}
}

// callbackHandler handles the response back from Steam. It verifies the callback and then renders
// the index template with the logged in user's id.
func callbackHandler(w http.ResponseWriter, r *http.Request) {
	fullURL := "http://" + domain + port + r.URL.String()

	id, err := openid.Verify(fullURL, discoveryCache, nonceStore)
	if err != nil {
		log.Printf("Error verifying: %q\n", err)
	} else {
		log.Printf("NonceStore: %+v\n", nonceStore)
		data := make(map[string]string)
		data["user"] = id

		profileId := strings.ReplaceAll(id, "https://steamcommunity.com/openid/id/", "")
		AjouteJoueur(profileId)
		data["games"] = AfficheJeuxCommuns()

		indexTemplate.Execute(w, data)
	}
}

func main() {

	//players := []string{"76561197996862080", "76561197967683837", "76561197985671488", "76561197972991790"}
	players := []string{"76561197967683837", "76561197985671488", "76561197972991790"}
	ownershipInfo = make(map[string]SteamOwnershipInfo)

	for _, player := range players {
		AjouteJoueur(player)
	}

	//fmt.Printf("%+v\n", ownershipInfo)

	//for _, game := range ownershipInfo {
	//	if len(game.Ownership) > 2 {
	//		fmt.Printf("%s est possédé par %d joueurs : %s\n", game.GameInfo.Name, len(game.Ownership), strings.Join(game.Ownership, ";"))
	//	}
	//}

	AfficheJeuxCommuns()

	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/discover", discoverHandler)
	http.HandleFunc("/openidcallback", callbackHandler)
	http.ListenAndServe(port, nil)
}

func AjouteJoueur(player string) {
	err, name, gameList := GetGamesFromProfileId(player)
	if err != nil {
		fmt.Printf("Erreur en lisant le profil %s : %v\n", player, err)
		return
	}
	if gameList != nil {
		// On connait le joueur, et sa liste de jeux
		fmt.Printf("%s possède %d jeux!\n", name, len(gameList))

		for _, game := range gameList {
			info, exists := ownershipInfo[game.ApplicationId]
			if !exists {
				data := JsonGameData{}
				if game.ApplicationId == "346110" {
					err, data = game.GetInfo()
					if err != nil {
						data = JsonGameData{}
					}
				}
				ownershipInfo[game.ApplicationId] = SteamOwnershipInfo{
					GameInfo:  game,
					GameData:  data,
					Ownership: []string{name},
				}
			} else {
				info.Ownership = append(info.Ownership, name)
				ownershipInfo[game.ApplicationId] = info
			}
		}

	} else {
		fmt.Printf("%s possède un profil non public\n", name)
	}

}

type WebGameInfo struct {
	Name   string   `json:"name"`
	Owners []string `json:"owners"`
	Tags   []string `json:"tags"`
}

type WebGames struct {
	Games []WebGameInfo `json:"games"`
}

func AfficheJeuxCommuns() string {
	sortingList := make([]SteamOwnershipInfo, 0)
	for _, v := range ownershipInfo {
		sortingList = append(sortingList, v)
	}

	sort.Slice(sortingList, func(i, j int) bool {
		return len(sortingList[i].Ownership) > len(sortingList[j].Ownership)
	})

	result := WebGames{Games: make([]WebGameInfo, 0)}

	for _, game := range sortingList {
		tags := make([]string, 0)
		tmp := WebGameInfo{
			Name:   game.GameInfo.Name,
			Owners: game.Ownership,
		}
		if game.GameData.Name != "" {
			for _, val := range game.GameData.Categories {
				tags = append(tags, val.Description)
			}
		}
		tmp.Tags = tags
		result.Games = append(result.Games, tmp)
		fmt.Printf("%s est possédé par %d joueurs : %s\n", game.GameInfo.Name, len(game.Ownership), strings.Join(game.Ownership, ";"))
	}
	data, err := json.Marshal(result)
	if err != nil {
		return "{}"
	}
	return string(data)
}
